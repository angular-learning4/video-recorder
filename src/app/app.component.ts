import { CommonModule } from '@angular/common';
import { Component, ElementRef, ViewChild } from '@angular/core';
import { RouterOutlet } from '@angular/router';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [RouterOutlet,CommonModule],
  templateUrl: './app.component.html',
  styleUrl: './app.component.css'
})
export class AppComponent {
  title = 'video-recording-app';
  @ViewChild('videoElement', { static: true }) videoElement!: ElementRef<HTMLVideoElement>;
  mediaRecorder: MediaRecorder | null = null;
  recordedBlobs: Blob[] = [];
  recordedVideoUrl: string = '';

  async ngOnInit() {
    await this.initCamera();
  }

  async initCamera() {
    const constraints = {
      video: true,
      audio: true
    };

    try {
      const stream = await navigator.mediaDevices.getUserMedia(constraints);
      this.videoElement.nativeElement.srcObject = stream;
    } catch (error) {
      console.error('Error accessing media devices.', error);
    }
  }

  startRecording() {
    const videoElement = this.videoElement.nativeElement;
    const stream = videoElement.srcObject as MediaStream;

    if (!stream) {
      console.error('No stream found on video element');
      return;
    }

    this.recordedBlobs = [];
    const options = { mimeType: 'video/webm;codecs=vp9' };

    try {
      this.mediaRecorder = new MediaRecorder(stream, options);
    } catch (e) {
      console.error('Exception while creating MediaRecorder:', e);
      return;
    }

    this.mediaRecorder.onstop = (event) => {
      console.log('Recorder stopped: ', event);
      const superBuffer = new Blob(this.recordedBlobs, { type: 'video/webm' });
      this.recordedVideoUrl = window.URL.createObjectURL(superBuffer);
    };

    this.mediaRecorder.ondataavailable = (event) => {
      if (event.data && event.data.size > 0) {
        this.recordedBlobs.push(event.data);
      }
    };

    this.mediaRecorder.start();
    console.log('MediaRecorder started', this.mediaRecorder);
  }

  stopRecording() {
    if (this.mediaRecorder) {
      this.mediaRecorder.stop();
      console.log('Recorded Blobs: ', this.recordedBlobs);
    }
  }

  downloadRecording() {
    const blob = new Blob(this.recordedBlobs, { type: 'video/webm' });
    const url = window.URL.createObjectURL(blob);
    const a = document.createElement('a');
    a.style.display = 'none';
    a.href = url;
    a.download = 'test.webm';
    document.body.appendChild(a);
    a.click();
    setTimeout(() => {
      document.body.removeChild(a);
      window.URL.revokeObjectURL(url);
    }, 100);
  }
}
